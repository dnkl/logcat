#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <sys/types.h>
#include <regex.h>

#include "version.h"

static void
print_usage(const char *prog_name)
{
    printf("Usage: %s [OPTIONS] [file...]\n", prog_name);
    printf("\n");
    printf("Options:\n");
    printf("  file                        file(s) to cat. If no files are specified, read from stdin\n");
    printf("  -p,--priority               don't remove 'priority' from the highlighted log lines\n");
    printf("  -e,--no-emoji               don't convert facility names to emojis\n");
    printf("  -v,--version                show the version number and quit\n");
}


static const char *
priority_color(const char *priority, size_t n)
{
    if (strncmp(priority, "debug", n) == 0)        return "\033[2m";       /* dim */
    else if (strncmp(priority, "info", n) == 0)    return "";
    else if (strncmp(priority, "notice", n) == 0)  return "\033[1m";       /* bold */
    else if (strncmp(priority, "warning", n) == 0) return "\033[33m";      /* yellow */
    else if (strncmp(priority, "err", n) == 0)     return "\033[31m";      /* red */
    else if (strncmp(priority, "crit", n) == 0)    return "\033[31;1m";    /* bold red */
    else if (strncmp(priority, "alert", n) == 0)   return "\033[31;1;4m";  /* bold red, underlined */
    else if (strncmp(priority, "emerg", n) == 0)   return "\033[31;1;7m";  /* bold red, inverted */

    return "";
}

static void
cat_one(FILE *f, regex_t *re, bool emit_priority, bool emit_emoji)
{
    char *line = NULL;
    size_t len = 0;

    for (ssize_t count = getline(&line, &len, f);
         count != -1;
         count = getline(&line, &len, f))
    {
        regmatch_t matches[re->re_nsub + 1];
        if (regexec(re, line, re->re_nsub + 1, matches, 0) == REG_NOMATCH ||
            matches[0].rm_so != 0)
        {
            printf("%s", line);
            continue;
        }

        const regmatch_t *timestamp = &matches[1];
        //const regmatch_t *no_facility = &matches[2];
        const regmatch_t *facility = &matches[3];
        const regmatch_t *priority = &matches[4];
        const regmatch_t *program_pid = &matches[5];
        const regmatch_t *program = &matches[6];
        const regmatch_t *pid = &matches[7];
        const regmatch_t *msg = &matches[8];

        /* Fields we expect to *always* exist */
        assert(timestamp->rm_so >= 0);
        assert(priority->rm_so >= 0);
        assert(program_pid->rm_so == -1 || (program->rm_so >= 0 && pid->rm_so >= 0));
        assert(msg->rm_so >= 0);

        /* Timestamp */
        fwrite("\033[32m", 1, 5, stdout);
        fwrite(&line[timestamp->rm_so], 1, timestamp->rm_eo - timestamp->rm_so, stdout);
        fwrite("\033[39m", 1, 6, stdout);

        bool emit_facility_or_priority = facility->rm_so >= 0 || emit_priority;

        /* [facility|priority] */
        if (emit_facility_or_priority) {
            fwrite(" \033[2m", 1, 5, stdout);

            if (facility->rm_so >= 0) {
                const char *fac = &line[facility->rm_so];
                size_t len = facility->rm_eo - facility->rm_so;

                if (emit_emoji) {
                    if (strncmp(fac, "daemon", len) == 0)
                        fwrite("👻", 1, strlen("👻"), stdout);
                    else if (strncmp(fac, "cron", len) == 0)
                        fwrite("⌛", 1, strlen("⌛"), stdout);
                    else if (strncmp(fac, "user", len) == 0)
                        fwrite("🧑", 1, strlen("🧑"), stdout);
                    else if (strncmp(fac, "mail", len) == 0)
                        fwrite("📫", 1, strlen("📫"), stdout);
                    else if (strncmp(fac, "news", len) == 0)
                        fwrite("📰", 1, strlen("📰"), stdout);
                    else if (strncmp(fac, "ftp", len) == 0)
                        fwrite("📁", 1, strlen("📁"), stdout);
                    else if (strncmp(fac, "auth", len) == 0 || strncmp(fac, "authpriv", len) == 0)
                        fwrite("🔑", 1, strlen("🔑"), stdout);
                    else
                        fwrite(fac, 1, len, stdout);
                } else
                    fwrite(fac, 1, len, stdout);

                if (emit_priority)
                    fwrite("|", 1, 1, stdout);
            } else {
                if (emit_emoji) {
                    /* Align */
                    fwrite("  ", 1, 2, stdout);
                }
            }

            if (emit_priority)
                fwrite(&line[priority->rm_so], 1, priority->rm_eo - priority->rm_so, stdout);

            fwrite("\033[22m", 1, 5, stdout);
        }

        /* Program[pid] */
        if (program_pid->rm_so >= 0) {
            fwrite(" \033[34m", 1, 6, stdout);
            fwrite(&line[program->rm_so], 1, program->rm_eo - program->rm_so, stdout);
            fwrite("\033[39m[\033[36m", 1, 11, stdout);
            fwrite(&line[pid->rm_so], 1, pid->rm_eo - pid->rm_so, stdout);
            fwrite("\033[39m]", 1, 6, stdout);
        }

        fwrite(": ", 1, 2, stdout);

        const char *clr = priority_color(
            &line[priority->rm_so], priority->rm_eo - priority->rm_so);
        fwrite(clr, 1, strlen(clr), stdout);
        fwrite(&line[msg->rm_so], 1, msg->rm_eo - msg->rm_so, stdout);
        fwrite("\033[m\n", 1, 4, stdout);
    }

    free(line);
}

int
main(int argc, char *const *argv)
{
    const char* const prog_name = argv[0];
    static const struct option longopts[] = {
        {"priority", no_argument, 0, 'p'},
        {"no-emoji", no_argument, 0, 'e'},
        {"version",  no_argument, 0, 'v'},
        {"help",     no_argument, 0, 'h'},
        {NULL,       no_argument, 0,   0},
    };

    bool emit_priority = false;
    bool emit_emoji = true;

    while (true) {
        int c = getopt_long(argc, argv, ":pevh", longopts, NULL);
        if (c == -1)
            break;

        switch (c) {
        case 'p':
            emit_priority = true;
            break;

        case 'e':
            emit_emoji = false;
            break;

        case 'v':
            printf("logcat version %s\n", LOGCAT_VERSION);
            return EXIT_SUCCESS;

        case 'h':
            print_usage(prog_name);
            return EXIT_SUCCESS;

        case ':':
            fprintf(stderr, "error: -%c: missing required argument\n", optopt);
            return EXIT_FAILURE;

        case '?':
            fprintf(stderr, "error: -%c: invalid option\n", optopt);
            return EXIT_FAILURE;
        }
    }

    argc -= optind;
    argv += optind;

    regex_t re;
    int r = regcomp(
        &re,
        /* Timestamp */
        "\\([^\\[]\\+\\) "

        /* [facility|priority] OR [priority] */
        "\\(\\[\\(auth\\|authpriv\\|cron\\|daemon\\|ftp\\|kern\\|local\\|lpr\\|mail\\|news\\|syslog\\|user\\|uucp\\)|\\|\\[\\)"
        "\\(debug\\|info\\|notice\\|warning\\|err\\|crit\\|alert\\|emerg\\)\\]"

        /* program[pid] (optional) */
        "\\( \\([^\\[]\\+\\)\\[\\([0-9]*\\)\\]\\)\\?"

        /* msg */
        ": \\(.*\\)",
        REG_NEWLINE);

    if (r != 0) {
        char err[1024];
        regerror(r, &re, err, sizeof(err));
        fprintf(stderr, "error; failed to compile regex: %s\n", err);
        regfree(&re);
        return EXIT_FAILURE;
    }

    if (argc == 0) {
        /* Read from stdin */
        cat_one(stdin, &re, emit_priority, emit_emoji);
    }

    for (int i = 0; i < argc; i++) {
        FILE *f = fopen(argv[i], "r");
        if (f == NULL) {
            fprintf(stderr, "error: %s: failed to open: %s\n",
                    argv[i], strerror(errno));
            continue;
        }

        cat_one(f, &re, emit_priority, emit_emoji);
        fclose(f);
    }

    regfree(&re);
    return EXIT_SUCCESS;
}
