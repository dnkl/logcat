# logcat

**logcat** is a small utility that parses syslog files and prints them
to stdout, highlighted with ANSI escape sequences.

The regex used to match the various parts of a log message is
currently hard coded and intended to match log messages on the
following format:

* `$DATE [$FACILITY|$PRIORITY] $PROGRAM[$PID]: $MSG`
* `$DATE [$PRIORITY]: $MSG`

In _syslog-ng_, this can be achieved with `template()`, in
_destination_ definitions.

Example:

    destination d_kernel { file("/var/log/kernel.log" template("$DATE [$PRIORITY]: $MSG\n")); };
    destination d_system { file("/var/log/system.log" template("$DATE [$FACILITY|$PRIORITY] $PROGRAM[$PID]: $MSG\n")); };
